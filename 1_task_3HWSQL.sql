--Создание таблиц

CREATE TABLE users (
user_id SERIAL PRIMARY KEY,
birth_date DATE,
sex CHAR(1) CHECK (sex IN ('M', 'F')),
age INTEGER CHECK (age >= 0)
);

CREATE TABLE items (
item_id SERIAL PRIMARY KEY,
description VARCHAR(1000),
price DECIMAL(10,2) CHECK (price >= 0),
category VARCHAR(300),
date_added DATE 
);

CREATE TABLE ratings (
id_rew SERIAL PRIMARY KEY, 
item_id INTEGER REFERENCES items(item_id),
user_id INTEGER REFERENCES users(user_id),
review VARCHAR(1000),
rating SMALLINT CHECK (rating BETWEEN 1 AND 5),
created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

--Генерация данных

WITH generated_users AS (
SELECT
date_trunc('year', CURRENT_DATE) - interval '1 year' * floor(random() * 80)::int
+ interval '1 day' * floor(random() * 365)::int AS random_birth_date,
CASE WHEN floor(random() * 2)::int = 0 THEN 'M' ELSE 'F' END AS random_sex
FROM generate_series(1, 20)
)

INSERT INTO users (birth_date, sex, age)
SELECT
random_birth_date,
random_sex,
EXTRACT(YEAR FROM age(CURRENT_DATE, random_birth_date))::INTEGER
FROM generated_users;

INSERT INTO items (description, price, category, date_added)
SELECT
md5(random()::text),
round((random() * 500)::numeric, 2),
unnest(array['Electronics', 'Clothing', 'Books', 'Groceries']),
DATE '2023-09-01' + interval '1 day' * floor(random() * 30)::int 
FROM generate_series(1, 20);

INSERT INTO ratings (item_id, user_id, review, rating)
SELECT
(SELECT item_id FROM items ORDER BY random() LIMIT 1),
(SELECT user_id FROM users ORDER BY random() LIMIT 1),
md5(random()::text),
floor(random() * 5 + 1)::int
FROM generate_series(1, 20);

/*
Описание:

users:

user_id - Тип SERIAL подойдет для автоинкрементных уникальных идентификаторов пользователей.
birth_date - Для хранения дат лучше всего подходит DATE
sex - используем CHAR(1), так как это текстовый тип данных состоящий из 1 символа
age - возраст это целые значения, которые лучше хранить в INTEGER. Но я бы не советовал оставлять эту колонку, 
так как мы можем получить в любой момент возраст при помощи колонки birth_date и этот возраст будет актуален, в отличии от этой колонки.

items:

item_id - Тоже самое, что и user_id. Уникальные id товаров удобно хранить в SERIAL.
description - Предположим, что описание товара не может быть более 1000 символов, тогда воспользуемся VARCHAR, 
что бы хранить строковые данные. Можно воспользоваться так же TEXT.
price - цена, это не целочисленное числовое значение, нам достаточно округление до 2 символов, поэтому DECIMAL(10,2)
category - категория товара, это строковые данные. Предположим, что ограничение в 300 символов VARCHAR(300)
date_added - предлагаю добавить дату добавления товара. Может пригодиться для сортировки или анализа. Даты храним в DATE.

ratings:

Добавил id отзыва, это может пригодиться для лучшего взаимодействия с данными. 
К примеру придаст уникальность отзывам, в случае если один user оставит несколько отзывов к одному item, мы сможем отличить их и сослаться при необходимости.
item_id и user_id это первичные целочисленные ключи в других таблицах, поэтому используем INTEGER REFERENCES
review  - тоже самое, что и для описания товара. Используем текстовое значнеие VARCHAR(1000). Предположим, что ограничение в 1000 символов.
rating - Так как это небольшое количество целочисленных значений, мы можем воспользоваться SMALLINT
created_at - Так же я добавил колонку с временем добавления отзыва. Это может пригодиться для анализа и лучшего хранения. Для хранения времени используем TIMESTAMP. 
При использовании DEFAULT CURRENT_TIMESTAMP мы будем указывать текущее время, если не указанно иное при добавлении.*/
